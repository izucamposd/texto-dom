function receber_msg (){
    let input_msg = document.querySelector(".digitar_msg");
    let texto = document.createElement("p");
    let escopo = document.createElement("div");
    let botao_excluir = document.createElement("button");
    botao_excluir.setAttribute("onclick", "this.parentNode.style.display = 'none'");
    let botao_editar = document.createElement("button");
    texto.innerText = input_msg.value; 
    texto.classList.add("texto1");
    input_msg.value = "";
    botao_excluir.innerText = "Excluir";
    botao_editar.innerText = "Editar";
    escopo.classList.add("div_mensagem");
    botao_excluir.classList.add("enviar_msg2"); 
    botao_editar.classList.add("enviar_msg2"); 
    escopo.append(texto);
    escopo.append(botao_excluir);
    escopo.append(botao_editar);
    let quadro = document.querySelector(".historico");
    quadro.append(escopo);
}

let btn_enviar = document.querySelector("#enviar_msg");
btn_enviar.addEventListener("click", receber_msg);
